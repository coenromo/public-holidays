//Require Needle for http requests; tunnel for creating agent for https and proxy
const request = require('needle');
const tunnel = require('tunnel');

//Create Proxy agent using tunnel
try {
var myAgent = tunnel.httpsOverHttp({
    proxy: {
      host: 'host-url',
      port: 8888,
      proxyAuth: process.env.vUser +':'+ process.env.vPass
    }
});
} catch(err) {
    console.log(err.message);
    process.exit(1);
};
//Get data using a needle GET call and tunnel agent
//public API call to data.gov.au
var aPHol;
request('get', 'https://data.gov.au/data/api/3/action/datastore_search?resource_id=31eec35e-1de6-4f04-9703-9be1d43d405b', { agent: myAgent })
    .then(function(response) {
        console.log('Got data:', response.body.result.records.length, 'entries sucessfully GOT from data.gov.au');
        aPHol = response.body.result.records;
        const hu = require('./hanaUpload.js');
        hu(aPHol);

    })
    .catch(function(err) {
        console.log(err);
        process.exit(1);
    }) 
