# Public Holidays

Uses similar code to that in [locator](https://gitlab.com/coenromo/locator). Is just a simple script to retrieve Date, Name and State of Australian public holidays data from https://data.gov.au/ to ensure we have the most up to date information. 

## This project uses the following node modules:

* hdb - HANA Database client
* tunnel - Proxy agent
* needle - http request

## Setup for development

1. Clone repo
2. in cmd, cd into where you cloned the repo, type npm install
3. Start > search "environment variables". Click 'edit the environment variables'
4. Add the following:
    * vUser (universal username, for proxy login)
    * vPass (as above)
    * sUserHana (T3 HANA user name)
    * sPassHana ([base64](https://www.base64encode.org/) version of your hana password)
    * sEnv (environment, for development use t3d)
5. You will need to change the destination hana table if you wish to check results as mine will be restricted
6. have a code