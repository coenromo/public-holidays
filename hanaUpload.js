//hdb is a HANA Client Node Module, errors.js deals with user info errors (environment variables)
const hdb = require('hdb');
const errorHandler = require('./errors.js');

//Establist HANA DB Connection
var client = hdb.createClient({
    host: 'url' + errorHandler.sourceEnvironment + 'url',
    port: 33015,
    databaseName: errorHandler.sourceEnvironment,
    user: errorHandler.userHana,
    password: errorHandler.passwordHana
});
client.on('error', function (err) {
    console.error('Network connection error: ', err);
    process.exit(1);
});
client.connect(function (err) {
    if(err) {
    console.error('Connect error: ', err);
    process.exit(1);
    }
});

//Loops through array of Public Holidays and constructs SQL UPSERTs for each, Updates HANA table
var sSQL = 'UPSERT "DB"."DB.LINK::pHols.pHolsMaster" ("id", "Date", "Name", "Description", "State", "DateModified") VALUES (?,?,?,?,?,CURRENT_UTCTIMESTAMP) WITH PRIMARY KEY';
var aSQL = [];
var element = 0;
const uploadData = (aPHol) => {
    for (element of aPHol) {
        //Construct SQL
        aSQL.push([
            element._id, 
            element.Date, 
            element['Holiday Name'].replace("'","''"), 
            element.Information.replace("'","''"), 
            element.Jurisdiction]);
            };
        };
    
    //Action SQL Console Log Sucess, end HANA client session 
    client.prepare(sSQL, function(err, statement){
        if(err) {
            console.error("Error in preparation of statement: ", err);
            process.exit(1);
        }
        statement.exec(aSQL, function(err, rows){
            if(err) {
                console.error("Error in execution of statement: ", err);
                process.exit(1);
            }
            console.log('Updated', rows.length, 'entries in', errorHandler.sourceEnvironment, 'from data.gov.au');
            client.end();
            });
        }); 
module.exports = uploadData;