/*
** hana pass
*/
try {
    if(!process.env.sPassHana || !process.env.sUserHana) {
        console.log('HANA Credentials do not exist, have you updated your environment variables?');
        process.exit(1);
    }
    passwordHana = Buffer.from(process.env.sPassHana, 'base64').toString('ascii');
    userHana = process.env.sUserHana; 
} catch (error) {
    console.log('Password conversion error: ', error);
    process.exit(1);
}

/* 
** T3 Environments
*/
try {
    if(!process.env.sEnv) {
        console.log('T3 Environment has not been set, please confirm your upload destination.');
        process.exit(1);
    }
    sourceEnvironment = process.env.sEnv;
} catch (error) {
    console.log('T3 Environment Error: ', error);
    process.exit(1);
}

module.exports = {
    passwordHana: passwordHana,
    userHana: userHana,
    sourceEnvironment: sourceEnvironment }
